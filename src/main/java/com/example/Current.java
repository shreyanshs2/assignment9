package com.example;

import org.springframework.stereotype.Component;

@Component("current")
public class Current implements Account{
    public String showAccount(){
        return "current";
    }
}
