package com.example;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Customer {
    
    private String name;
    
    private String username;
    
    private String password;
    
    private String ssn;
    
    private String address;
    
    private String email;

    private int age;

    private long phone;

    private float balance;

    String accountType;


    @Autowired
    @Qualifier("savings")
    Account account;

    public void display(){

        System.out.println("the Account type is "+account.showAccount());
    }

}
