package com.example;

import org.springframework.stereotype.Component;

@Component("savings")
public class Savings implements Account {
    public String showAccount(){
        return "Savings";
    }
}
