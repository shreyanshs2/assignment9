# ASSIGNMENT 9

## Account.java

```java
package com.example;

public interface Account {
    public String showAccount();
}

```

## Savings.java

```java
package com.example;

import org.springframework.stereotype.Component;

@Component("savings")
public class Savings implements Account {
    public String showAccount(){
        return "Savings";
    }
}
```

## Current.java

```java
package com.example;

import org.springframework.stereotype.Component;

@Component("current")
public class Current implements Account{
    public String showAccount(){
        return "current";
    }
}
```

## Customer.java

```java
package com.example;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Customer {
    
    private String name;
    
    private String username;
    
    private String password;
    
    private String ssn;
    
    private String address;
    
    private String email;

    private int age;

    private long phone;

    private float balance;

    String accountType;


    @Autowired
    @Qualifier("savings")
    Account account;

    public void display(){

        System.out.println("the Account type is "+account.showAccount());
    }

}
```

## AppConfig.java

```java
package com.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.example")
public class AppConfig {
    @Bean
    public Customer getCustomer(){
        return new Customer();
    }
}
```

## App.java

```java
package com.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
            
        Customer customer1 = context.getBean("customer", Customer.class);
        customer1.display();
    }
}
```

## Output

```
F:\programmes\Antwalk\assignment9> cmd /C ""C:\Program Files\Eclipse Adoptium\jdk-8.0.322.6-hotspot\bin\java.exe" -cp C:\Users\ss\AppData\Local\Temp\cp_58sdj32ue7vix5tgopnjaw4x9.jar com.example.App "
Hello World!
the Account type is Savings

F:\programmes\Antwalk\assignment9>
```